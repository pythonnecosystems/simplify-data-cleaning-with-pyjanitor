# Python: Pyjanitor로 데이터 정리의 간소화 <sup>[1](#footnote_1)</sup>

> <font size="3">Pandas에서 개발된 데이터 정리를 위한 API</font>

데이터 정리는 모든 데이터 과학 프로젝트에서 필수적인 부분이다. 데이터가 깨끗하지 않으면 데이터에서 도출된 인사이트가 부정확할 가능성이 높다.

그러나 데이터 정리는 시간이 많이 걸리고 지루한 과정이 될 수 있으며, 종종 길고 복잡한 코드를 작성해야 하는 경우가 많다.

다행히도 Pyjanitor는 데이터 정리 프로세스를 간소화하여 데이터 과학자와 분석가가 더 쉽고 효율적으로 작업할 수 있게 해주는 강력한 라이브러리이다.

이 글에서는 Pyjanitor가 데이터 정리 프로세스를 간소화하는 데 어떻게 도움이 되는지 살펴보겠다. 먼저 Pyjanitor의 정의와 주요 기능에 대해 설명한다. 그런 다음 Pyjanitor를 사용하여 데이터를 정리하고 변환하는 방법에 대한 몇 가지 실용적인 예를 살펴볼 것이다.

끝날 때쯤이면 Pyjanitor를 사용하여 데이터 정리 워크플로우를 간소화하고 데이터를 분석하고 해석하는 데 더 많은 시간을 할애하는 방법을 확실히 이해하게 될 것이다.

<a name="footnote_1">1</a>: [Python: Simplify Your Data Cleaning with Pyjanitor](https://pravash-techie.medium.com/python-simplify-your-data-cleaning-with-pyjanitor-79176e9ee586)를 편역한 것이다. 