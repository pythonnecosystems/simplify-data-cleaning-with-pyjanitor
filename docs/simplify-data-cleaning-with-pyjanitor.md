# Python: Pyjanitor로 데이터 정리의 간소화

## Pyjanitor란?
Pyjanitor는 데이터 정리 프로세스를 간소화하는 Python 라이브러리이다. 이 라이브러리는 널리 사용되는 Pandas 라이브러리의 확장판으로, 데이터 정리와 준비를 위한 추가 기능을 제공한다.

Pyjanitor는 사용하기 쉽고 효율적이며 커스터마이즈가 가능하기 때문에 데이터 과학자와 분석가에게 인기가 높다.

Pyjanitor는 데이터 정리를 위한 다양한 기능을 제공하는 매우 다재다능한 라이브러리이다. Pyjanitor의 주요 기능은 다음과 같다.

- 열 추가와 제거
- 열 이름 바꾸기
- 누락된 값 처리
- 데이터 필터링
- 데이터 그룹화
- 데이터 재구성
- 문자열과 텍스트 데이터 처리

## Pyjanitor의 장점
데이터 정리를 위해 Pyjanitor를 사용하면 다음과 같은 주요 이점이 있다.

- 데이터 정리 프로세스 간소화
- 시간과 노력 절약
- 데이터 정리와 준비를 위한 다양한 기능 제공
- 고도의 커스터마이징과 유연성
- Pandas 및 기타 인기 있는 Python 라이브러리와 호환 가능

## Python에서 Pyjanitor 사용법
직원과 직원들의 급여에 대한 데이터 세트가 있다고 가정해 보겠다. 데이터 집합에 몇 가지 누락된 값이 있고 일부 열의 이름이 일관되지 않다.

**설치** -

```bash
$ pip install pyjanitor
```

Pyjanitor를 사용하여 데이터 세트를 정리하는 방법은 다음과 같다.

```python
import pandas as pd
import janitor

# Read the dataset
df = pd.read_csv('employees.csv')

# Clean the column names
df = df.clean_names()

# Fill missing values with the median salary
df = df.fill_median('salary')

# Droping the unnecessary columns
df = df.remove_columns(['ssn', 'dob'])

# Convert the salary to a float
df['salary'] = df['salary'].astype(float)

# Sort the dataframe by the salary column in descending order
df = df.sort_values(by='salary', ascending=False)

# Save the cleaned dataframe to a new CSV file
df.to_csv('cleaned_employees.csv', index=False)
```

이 예에서는 먼저 Pyjanitor를 비롯한 필요한 라이브러리를 임포트한다. 다음 Pandas의 `read_csv` 함수를 사용해 데이터 세트를 읽는다. 이후 Pyjanitor의 `clean_names` 함수를 사용하여 열 이름을 표준화하했다. 다음으로, `fill_median` 함수를 사용하여 급여 열의 누락된 값들 모두를 중간값 급여로 채웠다. 그리고 `remove_columns` 함수를 사용하여 불필요한 열을 제거했다.

`astype` 메서드를 사용하여 급여 열을 실수로 변환했다. 마지막으로, `sort_values` 메서드를 사용하여 데이터 프레임을 급여의 내림차순으로 정렬하고, 정리된 데이터 프레임을 `to_csv` 메서드를 사용하여 새 CSV 파일에 저장하였다.

다음은 Pyjanitor의 사용을 보여주는 또 다른 간단한 예이다.

```python
import pandas as pd
import janitor

df = pd.DataFrame({
    'Name': ['Alice', 'Bob', 'Charlie'],
    'Age': [25, None, 30],
    'Salary': [50000, 60000, 75000]
})

# cleaning operations with Pyjanitor
cleaned_df = (
    df.clean_names()  # Cleaning column names
      .remove_empty()  # Removing rows with missing values
      .set_index('name')  # Setting column-'name' as the index
      .rename_column('age', 'years_old')  # Renaming column-'age'
)

print(cleaned_df)

# output -
#         years_old  salary
# name                      
# Alice         25.0   50000
# Bob            NaN   60000
# Charlie       30.0   75000
```

## Pyjanitor의 다른 기능

### 데이터 재구성
데이터를 정리하는 것 외에도 Pyjanitor를 사용하여 데이터를 재구성하고 변형할 수 있다. Pyjanitor는 피벗, 멜팅, 열 분할 등 다양한 방법으로 데이터를 재구성할 수 있는 다양한 함수를 제공한다.

다음은 Pyjanitor를 사용하여 데이터를 재구성하는 방법의 예이다.

```python
import pandas as pd
import janitor

# Reading the dataset
df = pd.read_csv('my_dataset.csv')
```

데이터 집합에 `id`, `date`, `type_1`, `type_2`, `value_1`, `value_2` 열이 있다고 가정해 보자. 각 타입과 값 조합에 대해 별도의 열을 갖도록 데이터를 재구성하고 싶다. Pyjanitor의 `spread_columns()` 함수를 사용하면 이 작업을 수행할 수 있다.

```python
df = df.spread_columns(column_pairs=[('type_1', 'value_1'), ('type_2', 'value_2')], sep='_')

print(df.head())
```

```
OUTPUT -

   id        date  type_1_a  type_1_b  type_2_a  type_2_b
0   1  2021-01-01         3         4         5         6
1   2  2021-01-02         7         8         9        10
2   3  2021-01-03        11        12        13        14
```

각 타입과 값 조합에 대해 별도의 열을 사용하여 데이터가 재구성된 것을 볼 수 있다.

### 문자열과 텍스트 데이터 처리
다른 데이터 집합을 사용하여 문자열과 텍스트 데이터를 처리하는 방법을 살펴보겠다. 제목, 연도 및 장르를 포함한 영화에 대한 정보가 포함된 데이터 집합이 있다고 가정해 보자.
그러나 장르 열에는 대문자와 소문자 그리고 공백이 혼합되어 있다. 모든 장르가 제목 케이스(title case)로 표시되고 앞과 뒤에 공백이 없도록 장르 열을 표준화하려고 한다. Pyjanitor의 `clean_text()` 함수를 사용하면 이 작업을 수행할 수 있다.

```python
# Read the dataset
df = pd.read_csv('movies.csv')

# Clean the genre column
df = df.clean_names().clean_text('genre')

print(df.head())
```

```
OUTPUT -

                    title  year      genre
0           The Godfather  1972      Crime
1  The Shawshank Redemption  1994      Drama
2          The Dark Knight  2008     Action
3    The Godfather: Part II  1974      Crime
4              12 Angry Men  1957  Drama|Crime
```

장르 열이 표준화되어 모든 장르가 제목 케이스로 표시되고 앞과 뒤에 공백이 없는 것을 볼 수 있다.

## 마치며

Pyjanitor는 데이터 정리 프로세스를 단순화하고 자동화하는 일련의 기능을 제공하여 이 문제에 대한 해결책을 제시한다.

Pyjanitor는 데이터 정리 프로세스를 보다 빠르고 효율적으로 수행할 수 있는 강력하고 다재다능한 패키지이다.

Pyjanitor를 워크플로우에 통합하면 데이터 정리에 소요되는 시간을 줄이고 데이터 분석과 해석에 더 많은 시간을 할애할 수 있다.

따라서 데이터 정리 프로세스를 간소화하려는 경우, Pyjanitor를 사용해 보고 데이터 분석 기능을 향상시킬 수 있는 방법을 알아보세요.

## More Info.
- [A Complete Guide to Pyjanitor for Data Cleaning](https://www.analyticsvidhya.com/blog/2022/04/a-complete-guide-to-pyjanitor-for-data-cleaning/)
